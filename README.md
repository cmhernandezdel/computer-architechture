<h2> Computer architecture </h2>
N-body problem from the Computer Architecture course at Universidad Carlos III de Madrid.

Feel free to view, use and redistribute this code in any way.

<h3> N-body problem </h3>
From <a href="https://en.wikipedia.org/wiki/N-body_problem">wikipedia</a>: <br>
In physics, the n-body problem is the problem of predicting the individual motions of a group of celestial objects interacting with each other gravitationally.

This problem was solved sequentially <code>(nasteroids-seq)</code> and in parallel <code>(nasteroids-par)</code> with <a href="https://www.openmp.org/">Open MP</a> in C++ in 2017.