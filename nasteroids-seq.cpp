// compile with: g++ -std=c++14 -Wall -Wextra -Wno-deprecated -Werror -pedantic -pedantic-errors -O3 -DNDEBUG nasteroids-seq.cpp

/**
 * nasteroids-seq.cpp
 * Sequential solution to the n-body problem
 *
 * Solved in 2017 with C++
 * http://gitlab.com/cmhernandezdel
 * Author: Carlos M. Hernández Delgado
 * At Universidad Carlos III de Madrid
 */


#include <iostream>
#include <sstream>
#include <cctype>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <random>
#include <vector>
#include <chrono>

using namespace std;

/*** *** CONSTANT DEFINITIONS *** ***/

const double gravity = 6.674e-5;   // Universal gravitational constant
const double dt = 0.1;             // Time delta
const double dmin = 2.0;           // Minimum distance
const int width = 200;         		 // Width of space
const int height = 200;        		 // Height of space
const double f_max = 200.0;        // Maximum force
const double mass = 1000;          // Average mass
const double sdm = 50;             // Standard deviation of the mass
const int data_array_size = 3;		 // For checking

struct asteroide{	                 // Asteroid struct definition
  double x, y, masa, vx, vy;			 // X,Y coords, mass and X,Y speed
  bool eliminado;									 // true if the asteroid has been destroyed, false otherwise
};

/*** *** FUNCTION DEFINITIONS *** ***/

/* Checks if the given number is an integer greater or equal than 0
 * Returns 0 if the number meets the conditions, 1 otherwise
 */
int isInteger(char arg[]){
  for(unsigned int i = 0; i < strlen(arg); i++){
    if (!isdigit(arg[i])){
      return 1;
    }
  }
  return 0;
}

/* Checks if the given number is a double precision floating point number, either positive or negative
 * Returns 0 if the number meets the conditions, 1 otherwise
 */
int isDouble(char arg[]){
  int dotCounter = 0;
  for(unsigned int i = 0; i < strlen(arg); i++){
    if(arg[i] == '.') dotCounter++;
    if ((!isdigit(arg[i]) && arg[i] != '.' && arg[i] != '-') || (dotCounter > 1 && arg[i] == '.') || ((arg[i] == '-') && (i != 0)) )
      return 1;
    }
  return 0;
}

/* Checks if the arguments were passed correctly to the process */
int parseData(char* argv[], int data[], double &pos_rayo, unsigned int &semilla){

  /* Variable declarations
   * int[] parsedData: array containing the number of asteroids, iterations and planets
   * double parsedBeamPosition: position in Y axis of the laser beam
   * int parsedSeed: seed for map generation
   */
	
  int parsedData[data_array_size];
  long double parsedBeamPosition;
  unsigned int parsedSeed;
		
  for (int i = 0; i < data_array_size; i++){
    istringstream param(argv[i+1]);
    if( !(param >> parsedData[i]) || isInteger(argv[i+1]) ){
      return -1;
    }
    data[i] = parsedData[i];
  }

  istringstream paramBeam(argv[4]);
  if (!(paramBeam >> parsedBeamPosition) || isDouble(argv[4])){
    return -1;
  }
  pos_rayo = parsedBeamPosition;

  istringstream paramSeed(argv[5]);
  if ( !(paramSeed >> parsedSeed) || isInteger(argv[5]) ){
    return -1;
  }
  if(parsedSeed <= 0) return -1;
  semilla = parsedSeed;	
	

  return 0;

} 

/* Generates the initial configuration file, init_conf.txt */
int generateInputFile(std::vector<asteroide> &body, int num_asteroides, int num_iteraciones, int num_planetas, double beamPosition, int seed){
  FILE *f;
 
  f = fopen("init_conf.txt", "w"); // Open file and write the process' arguments
  fprintf(f, "%d %d %d %.3f %d\n", num_asteroides, num_iteraciones, num_planetas, beamPosition, seed);
	
  for(int i = 0; i < num_asteroides; i++){ // For every asteroid, write position and mass
    fprintf(f, "%.3f %.3f %.3f\n", body[i].x, body[i].y, body[i].masa); 
  }
	
  for(int j = 0; j < num_planetas; j++){ // For every planet, write position and mass
    fprintf(f, "%.3f %.3f %.3f\n", body[j+num_asteroides].x, body[j+num_asteroides].y, body[j+num_asteroides].masa); 
  }
	
  fprintf(f, "%.3f %.3f\n", 0.0, beamPosition); // Write laser beam position
		
	
  fclose(f);	// Close file
  return 0;
}
	
/* Generates the final output file, out.txt */
int generateOutputFile(std::vector<asteroide> &body, int num_asteroides){
  FILE *f;
	 
  f = fopen("out.txt", "w"); // Open file
	
  for(int i = 0; i < num_asteroides; i++){ // For every asteroid, write position, speed and mass
	
    if(!body[i].eliminado){ 
   	 fprintf(f, "%.3f %.3f %.3f %.3f %.3f\n", body[i].x, body[i].y, body[i].vx, body[i].vy, body[i].masa); 
    }
  }
		
	
  fclose(f);	// Close file
  return 0;
}

/*** *** PROGRAM START *** ***/

int main(int argc, char * argv[]){

	/* Variable declarations
	* int[] data: array containing number of asteroids(0), iterations(1) and planets(2)
	* double pos_rayo: position in Y axis of the laser beam
	* int semilla: seed used for map generation
	*/   

	int data[data_array_size];
	double pos_rayo;
	unsigned int semilla;


/* Check arguments */
if (argc != 6 || (parseData(argv, data, pos_rayo, semilla) == -1)){
	cerr << "Wrong arguments." << endl << "Correct use:" << endl << "nasteroids-seq num_asteroides num_iteraciones num_planetas pos_rayo semilla" << endl;
		return -1;
	}
	
	const int num_asteroides = data[0];  
	const int num_iteraciones = data[1];
	const int num_planetas = data[2];


	int i, j, iteracion; 							// For looping
	double pendiente, fx, fy, modf;		// Angles, distances and forces
	int n_cuerpos = num_asteroides+num_planetas;
 
	default_random_engine re{semilla};  // Set random values
	uniform_real_distribution<double> xdist{0.0, nextafter(width, numeric_limits<double>::max())};
	uniform_real_distribution<double> ydist{0.0, nextafter(height, numeric_limits<double>::max())};
	normal_distribution<double> mdist{mass, sdm};
	
	vector<asteroide> v_body(num_asteroides+num_planetas); // Vector with astronomical objects

	// Set random position and speed 0 to all asteroids
	for(i = 0; i < num_asteroides; i++){
		v_body[i] = {xdist(re), ydist(re), mdist(re), 0, 0, false};
	}
  
	// Set planet position to the edges of the space
	for(i = 0; i < num_planetas; i++){
		switch (i%4) {  
			case 0: 
				v_body[i+num_asteroides] = {0.0, ydist(re), 10*mdist(re), 0, 0, false};
				break;
			case 1: 
				v_body[i+num_asteroides] = {xdist(re), 0.0, 10*mdist(re), 0, 0, false}; 
				break;
			case 2: 
				v_body[i+num_asteroides] = {width, ydist(re), 10*mdist(re), 0, 0, false}; 
				break;
			case 3: 
				v_body[i+num_asteroides] = {xdist(re), height, 10*mdist(re), 0, 0, false}; 
		}
	}
 
	generateInputFile(v_body, num_asteroides, num_iteraciones, num_planetas, pos_rayo, semilla);	// Create initial file
	vector<double> fuerzas_x(num_asteroides); // Create vectors to store the sum of the forces in x and y
	vector<double> fuerzas_y(num_asteroides);
	double aceleracion_x, aceleracion_y, distanciaxy;
	n_cuerpos = num_asteroides+num_planetas;
	
	/* Start of iterations */

	for(iteracion = 0; iteracion < num_iteraciones; iteracion++){
	  	
		// Reset of force vectors: don't want to keep them between iterations, since asteroids can be destroyed
		// Optimization: it is slower if we do this in the same loop due to the spatial locality principle
		for(i=0; i < num_asteroides; i++){
			fuerzas_x[i]=0;
			fuerzas_y[i]=0;
		}
		
		// Calculation of forces that affect the asteroids
		for(i = 0; i < num_asteroides; i++){
			if(!v_body[i].eliminado){																																											// Iterate between alive asteroids
				for(j = i+1 ; j < n_cuerpos; j++){																																					// An asteroid doesn't apply force to itself
					if(!v_body[j].eliminado){																																									// Alive asteroids
						distanciaxy = sqrt((v_body[i].x - v_body[j].x)*(v_body[i].x - v_body[j].x)+(v_body[i].y - v_body[j].y)*(v_body[i].y - v_body[j].y));
						if((distanciaxy > dmin) || (j >= num_asteroides)){
							modf=gravity * v_body[i].masa * v_body[j].masa / (distanciaxy * distanciaxy);
							pendiente = ((v_body[i].y - v_body[j].y) / (v_body[i].x - v_body[j].x));
							if(((pendiente >= 1) || (pendiente <= -1)) && (pendiente != HUGE_VAL) && (pendiente != HUGE_VAL)) {		// If slope is greater than 1 or lesser than -1, truncate it
								pendiente = pendiente-trunc(pendiente);
							}
							if (modf > f_max){
								modf = f_max;	
							}
							if (modf < -f_max){
								modf = -f_max;							
							}

							fx = modf * cos(atan(pendiente));
							fy = modf * sin(atan(pendiente));
							fuerzas_x[i] = fuerzas_x[i] + fx;
							fuerzas_y[i] = fuerzas_y[i] + fy;
							if (j < num_asteroides){
								fuerzas_x[j] = fuerzas_x[j] - fx;
								fuerzas_y[j] = fuerzas_y[j] - fy;
							}	
						}
					}
				}

				aceleracion_x = fuerzas_x[i] / v_body[i].masa;			// Acceleration calculation
				aceleracion_y = fuerzas_y[i] / v_body[i].masa;
				v_body[i].vx = v_body[i].vx + aceleracion_x * dt;		// Speed calculation
				v_body[i].vy = v_body[i].vy + aceleracion_y * dt;
				v_body[i].x = v_body[i].x + v_body[i].vx * dt;			// Position calculation
				v_body[i].y = v_body[i].y + v_body[i].vy * dt;
			
				// Rebound effect
				if(v_body[i].x <= 0){
					v_body[i].x = dmin;
					v_body[i].vx = -v_body[i].vx;	
				}
			
				if(v_body[i].y <= 0){
					v_body[i].y = dmin;
					v_body[i].vy = -v_body[i].vy;
				}
			
				if(v_body[i].x >= width){
					v_body[i].x = width - dmin;
					v_body[i].vx = -v_body[i].vx;
				}
			
				if(v_body[i].y >= height){
					v_body[i].y = height - dmin;
					v_body[i].vy = -v_body[i].vy;
				}
				// If asteroid is inside beam, destroy it
				if((v_body[i].y >= pos_rayo - dmin) && (v_body[i].y <= pos_rayo + dmin)){
					v_body[i].eliminado = true;
				}
			}
		}
		// Once all forces in both axes for every asteroid are calculated, calculate accelerations, speeds and positions
	}  
  
	/* Generate output file */
	generateOutputFile(v_body, num_asteroides);
	return 0;
}
